<?php

/**
 * @file
 * Provide a custom views field data that isn't tied to any other module.
 */

/**
 * Implements hook_views_data_alter().
 */
function similar_reference_views_data_alter(&$data) {
  $table = 'views';

  $data[$table]['similar_ref_count'] = [
    'group' => t('Similar by reference field'),
    'title' => t('Similarity'),
    'help' => t('Percentage/count of references which entity has in common with entity given as argument.'),
    'field' => [
      'id' => 'similar_reference_field',
    ],
    'sort' => [
      'id' => 'similar_reference_sort',
    ],
  ];

  $data[$table]['similar_id'] = [
    'title' => t('ID'),
    'group' => t('Similar by reference field'),
    'help' => t('The ID of entity to base similarity off of.'),
    'argument' => [
      'id' => 'similar_reference_arg',
      'name field' => 'title',
      'numeric' => TRUE,
      'validate type' => 'nid',
    ],
  ];
}
