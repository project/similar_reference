<?php

namespace Drupal\similar_reference\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Shows the similarity of the entity.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("similar_reference_field")
 */
class SimilarReferenceField extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['count_type'] = ['default' => 1];
    $options['percent_suffix'] = ['default' => 1];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $form['count_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Display type'),
      '#default_value' => $this->options['count_type'],
      '#options' => [
        0 => $this->t('Show count of common references'),
        1 => $this->t('Show as percentage'),
      ],
    ];

    $form['percent_suffix'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Append % when showing percentage'),
      '#default_value' => !empty($this->options['percent_suffix']),
    ];
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing to overwrite parent behavior.
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $value = $values->{$this->field};

    if ($this->options['count_type'] == 0) {
      return $value;
    }
    elseif ($this->view->reference_ids) {
      $output = round($value / count($this->view->reference_ids) * 100);
      if (!empty($this->options['percent_suffix'])) {
        $output .= '%';
      }
      return $output;
    }
  }

}
