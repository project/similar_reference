<?php

namespace Drupal\similar_reference\Plugin\views\sort;

use Drupal\views\Plugin\views\sort\SortPluginBase;

/**
 * Handler which sort by the similarity.
 *
 * @ingroup views_sort_handlers
 *
 * @ViewsSort("similar_reference_sort")
 */
class SimilarReferenceSort extends SortPluginBase {

  /**
   * Define default sorting order.
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['order'] = ['default' => 'DESC'];
    return $options;
  }

  /**
   * Add orderBy.
   */
  public function query() {
    $this->ensureMyTable();
    $this->query->addOrderBy(NULL, NULL, $this->options['order'], 'similar_ref_count',);
  }

}
