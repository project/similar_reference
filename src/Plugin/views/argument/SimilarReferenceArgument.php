<?php

namespace Drupal\similar_reference\Plugin\views\argument;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\field\FieldStorageConfigInterface;
use Drupal\views\Plugin\views\argument\NumericArgument;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Argument handler to accept an entity id.
 *
 * @ingroup views_argument_handlers
 *
 * @ViewsArgument("similar_reference_arg")
 */
class SimilarReferenceArgument extends NumericArgument implements ContainerFactoryPluginInterface {

  /**
   * Field Alias.
   *
   * @var string
   */
  protected string $fieldAlias = 'similar_ref_count';

  /**
   * Database Service Object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $connection;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Constructs the SimilarTermsArgument object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Database\Connection $connection
   *   The datbase connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Connection $connection, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->connection = $connection;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition, $container->get('database'), $container->get('entity_type.manager'), $container->get('entity_field.manager'));
  }

  /**
   * Define default values for options.
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['reference_field'] = ['default' => NULL];
    $options['include_args'] = ['default' => FALSE];

    return $options;
  }

  /**
   * Build options settings form.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state): void {
    $entity_type = $this->getEntityType();
    if ($this->table == 'views') {
      $storage = $this->entityTypeManager->getStorage($entity_type);
      $this->table = $storage->getEntityType()->getBaseTable();
    }

    parent::buildOptionsForm($form, $form_state);

    $all_entity_fields = $this->entityFieldManager->getFieldStorageDefinitions($entity_type);
    $reference_fields = [];
    foreach ($all_entity_fields as $field_name => $field_storage_config) {
      if ($field_storage_config->getType() === 'entity_reference' && !$field_storage_config->isBaseField()) {
        $reference_fields[$field_storage_config->getLabel()] = $field_storage_config->getLabel();
      }
    }

    $form['reference_field'] = [
      '#type' => 'radios',
      '#required' => TRUE,
      '#title' => $this->t('Select field to compare on'),
      '#options' => $reference_fields,
      '#default_value' => empty($this->options['reference_field']) ? NULL : $this->options['reference_field'],
    ];

    $form['include_args'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include argument entity/entities in results'),
      '#description' => $this->t('If selected, the entity/entities passed as the argument will be included in the view results.'),
      '#default_value' => !empty($this->options['include_args']),
    ];
  }

  /**
   * Validate this argument works. By default, all arguments are valid.
   */
  public function validateArgument($arg) {
    if (isset($this->argument_validated)) {
      return $this->argument_validated;
    }

    $this->value = [$arg => $arg];
    $reference_field = $this->options['reference_field'];
    if (empty($reference_field)) {
      return FALSE;
    }

    $field = $this->entityTypeManager->getStorage('field_storage_config')
      ->load($reference_field);

    if (!$field instanceof FieldStorageConfigInterface) {
      return FALSE;
    }

    // @todo Get table name through table mapping.
    $table = str_replace('.', '__', $reference_field);
    $this->table = $table;
    // @todo Get column name through table mapping.
    $column = $field->getName() . '_target_id';
    $this->column = $column;

    $select = $this->connection->select($table, 'rf')->fields('rf', [$column]);
    $select->condition('rf.entity_id', $this->value, 'IN');
    $result = $select->execute();
    $this->reference_ids = [];
    foreach ($result as $row) {
      $this->reference_ids[$row->{$column}] = $row->{$column};
    }

    if (count($this->reference_ids) == 0) {
      return FALSE;
    }
    $this->view->reference_ids = $this->reference_ids;
    return TRUE;
  }

  /**
   * Add filter(s).
   */
  public function query($group_by = FALSE) {
    $this->ensureMyTable();

    $this->query->addTable($this->table, NULL, NULL, $this->table);
    $this->query->addField($this->table, 'entity_id', $this->fieldAlias, ['function' => 'count']);
    $this->query->addWhere(0, $this->table . '.' . $this->column, $this->reference_ids, 'IN');
    // Exclude the current entity/entities.
    if (empty($this->options['include_args'])) {
      $this->query->addWhere(0, $this->table . ".entity_id", $this->value, 'NOT IN');
    }
    $this->query->addGroupBy($this->table . '.entity_id');
  }

}
